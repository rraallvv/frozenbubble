/*
 *                 [[ Frozen-Bubble ]]
 *
 * Copyright (c) 2000-2003 Guillaume Cottenceau.
 * Java sourcecode - Copyright (c) 2003 Glenn Sanson.
 * Additional source - Copyright (c) 2013 Eric Fortin.
 *
 * This code is distributed under the GNU General Public License
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 or 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 * Free Software Foundation, Inc.
 * 675 Mass Ave
 * Cambridge, MA 02139, USA
 *
 * Artwork:
 *    Alexis Younes <73lab at free.fr>
 *      (everything but the bubbles)
 *    Amaury Amblard-Ladurantie <amaury at linuxfr.org>
 *      (the bubbles)
 *
 * Soundtrack:
 *    Matthias Le Bidan <matthias.le_bidan at caramail.com>
 *      (the three musics and all the sound effects)
 *
 * Design & Programming:
 *    Guillaume Cottenceau <guillaume.cottenceau at free.fr>
 *      (design and manage the project, whole Perl sourcecode)
 *
 * Java version:
 *    Glenn Sanson <glenn.sanson at free.fr>
 *      (whole Java sourcecode, including JIGA classes
 *             http://glenn.sanson.free.fr/jiga/)
 *
 * Android port:
 *    Pawel Aleksander Fedorynski <pfedor@fuw.edu.pl>
 *    Eric Fortin <videogameboy76 at yahoo.com>
 *    Copyright (c) Google Inc.
 *
 *          [[ http://glenn.sanson.free.fr/fb/ ]]
 *          [[ http://www.frozen-bubble.org/   ]]
 */

package org.jfedor.frozenbubble;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import com.efortin.frozenbubble.HighscoreManager;
import com.getnimapp.frozenbubble.R;

import java.util.Random;
import java.util.Vector;

public class FrozenGame extends GameScreen {

	public final static String PREFS_NAME   = "frozenbubble";
	public static boolean arcadeGame = false;

	private final int[] columnX = {190, 206, 222, 238, 254,
			270, 286, 302, 318, 334,
			350, 366, 382, 398, 414};

	public final static int HORIZONTAL_MOVE = 0;
	public final static int FIRE            = 1;

	public final static double LAUNCH_DIRECTION_MIN_STEP = 0.25d;
	public final static double LAUNCH_DIRECTION_STEP     = 0.5d;
	public final static double MIN_LAUNCH_DIRECTION      = 1.0d;
	public final static double START_LAUNCH_DIRECTION    = 20.0d;
	public final static double MAX_LAUNCH_DIRECTION      = 39.0d;

	public final static int KEY_UP    = 38;
	public final static int KEY_LEFT  = 37;
	public final static int KEY_RIGHT = 39;

	public static final int HURRY_ME_TIME = 480;

	double launchBubblePosition;
	LaunchBubbleSprite launchBubble;

	int currentColor;
	int nextColor;
	ImageSprite nextBubble;

	BmpWrap          gameLost;
	BmpWrap          gameWon;
	BmpWrap          penguins;
	BmpWrap[]        bubbles;
	BmpWrap[]        frozenBubbles;
	BmpWrap[]        targetedBubbles;
	BubbleSprite     movingBubble;
	BubbleManager    bubbleManager;
	BubbleSprite[][] bubblePlay;
	BubbleSprite[]   scrolling;
	Compressor       compressor;
	Drawable         launcher;
	LevelManager     levelManager;
	HighscoreManager highscoreManager;
	ImageSprite      pauseButtonSprite;
	ImageSprite      playButtonSprite;
	PenguinSprite    penguin;
	Random           random;
	SoundManager     soundManager;

	Vector<Sprite> falling;
	Vector<Sprite> goingUp;
	Vector<Sprite> jumping;

	BmpWrap bubbleBlink;
	int blinkDelay;

	ImageSprite hurrySprite;
	int hurryTime;

	boolean endOfGame;
	boolean frozenify;
	boolean isArcade;
	boolean readyToFire;
	boolean swapPressed;
	gameEnum playResult;
	int fixedBubbles;
	int frozenifyX, frozenifyY;
	int nbBubbles;

	public FrozenGame(BmpWrap[] bubbles_arg,
					  BmpWrap[] frozenBubbles_arg,
					  BmpWrap[] targetedBubbles_arg,
					  BmpWrap bubbleBlink_arg,
					  BmpWrap gameWon_arg,
					  BmpWrap gameLost_arg,
					  BmpWrap hurry_arg,
					  BmpWrap pauseButton_arg,
					  BmpWrap playButton_arg,
					  BmpWrap penguins_arg,
					  BmpWrap compressorHead_arg,
					  BmpWrap compressor_arg,
					  Drawable launcher_arg,
					  SoundManager soundManager_arg,
					  LevelManager levelManager_arg,
					  HighscoreManager highscoreManager_arg) {
		random               = new Random(System.currentTimeMillis());
		launcher             = launcher_arg;
		penguins             = penguins_arg;
		bubbles              = bubbles_arg;
		frozenBubbles        = frozenBubbles_arg;
		targetedBubbles      = targetedBubbles_arg;
		bubbleBlink          = bubbleBlink_arg;
		gameWon              = gameWon_arg;
		gameLost             = gameLost_arg;
		soundManager         = soundManager_arg;
		levelManager         = levelManager_arg;
		highscoreManager     = highscoreManager_arg;
		playResult           = gameEnum.PLAYING;
		launchBubblePosition = START_LAUNCH_DIRECTION;
		readyToFire          = false;
		swapPressed          = false;

		/*
		 * Initialize game modifier variables.
		 */
		isArcade = arcadeGame;

		/*
		 * Create objects for all the game graphics.
		 */
		if (pauseButton_arg != null) {
			pauseButtonSprite = new ImageSprite(new Rect(167, 444, 32, 32),
					pauseButton_arg);
			this.addSprite(pauseButtonSprite);
		}
		else {
			pauseButtonSprite = null;
		}

		if (playButton_arg != null) {
			playButtonSprite = new ImageSprite(new Rect(167, 444, 32, 32),
					playButton_arg);
		}
		else {
			playButtonSprite  = null;
		}

		penguin = new PenguinSprite(getPenguinRect(), penguins_arg, random);
		this.addSprite(penguin);

		compressor   = new Compressor(compressorHead_arg, compressor_arg);
		hurrySprite  = new ImageSprite(new Rect(203, 265, 203 + 240, 265 + 90),
				hurry_arg);

		falling = new Vector<Sprite>();
		goingUp = new Vector<Sprite>();
		jumping = new Vector<Sprite>();

		bubblePlay    = new BubbleSprite[LevelManager.NUM_COLS]
				[LevelManager.NUM_ROWS];
		scrolling     = new BubbleSprite[LevelManager.NUM_COLS];
		bubbleManager = new BubbleManager(bubbles);

		/*
		 * Load the current level to the bubble play grid.
		 */
		byte[][] currentLevel = levelManager.getCurrentLevel();
		if (currentLevel == null) {
			//Log.i("frozen-bubble", "Level not available.");
			return;
		}

		for (int j = 0; j < (LevelManager.NUM_ROWS - 1); j++) {
			for (int i = j%2; i < LevelManager.NUM_COLS; i++) {
				if (currentLevel[i][j] != -1) {
					BubbleSprite newOne = new BubbleSprite(
							new Rect(190+i*32-(j%2)*16, 44+j*28, 32, 32),
							currentLevel[i][j],
							bubbles[currentLevel[i][j]],
							frozenBubbles[currentLevel[i][j]], bubbleBlink, bubbleManager,
							soundManager, this);
					bubblePlay[i][j] = newOne;
					this.addSprite(newOne);
				}
			}
		}

		if (isArcade) {
			addScrollRow();
		}

		/*
		 * Initialize the launch bubbles.
		 */
		currentColor = bubbleManager.nextBubbleIndex(random);
		nextColor    = bubbleManager.nextBubbleIndex(random);

		nextBubble = new ImageSprite(new Rect(302, 440, 302 + 32, 440 + 32),
				bubbles[nextColor]);

		this.addSprite(nextBubble);
		launchBubble = new LaunchBubbleSprite(currentColor,
				launchBubblePosition,
				launcher, bubbles);
		this.spriteToBack(launchBubble);

		/*
		 * Initialize game metrics.
		 */
		nbBubbles      = 0;
	}

	public void addFallingBubble(BubbleSprite sprite) {
		spriteToFront(sprite);
		falling.addElement(sprite);
	}

	public void addJumpingBubble(BubbleSprite sprite) {
		spriteToFront(sprite);
		jumping.addElement(sprite);
	}

	private void addScrollRow() {
		byte[] newRow = levelManager.getNewRow(bubblePlay);
		int colIdx = (levelManager.getRowOffset() + 1) % 2;
		int rowMove = (int) compressor.getMoveDown();
		for (int column = 0; column < LevelManager.NUM_COLS; column++) {
			scrolling[column] = null;
		}
		for (int column = colIdx; column < LevelManager.NUM_COLS; column++) {
			if (newRow[column] != -1) {
				int color = newRow[column];
				BubbleSprite tempBubble = new BubbleSprite(
						new Rect(columnX[colIdx], 44 - 28 + rowMove, 32, 32),
						color, bubbles[color], frozenBubbles[color],
						bubbleBlink, bubbleManager, soundManager, this);
				scrolling[column] = tempBubble;
				this.addSprite(tempBubble);
				this.spriteToBack(tempBubble);
			}
			colIdx += 2;
		}
	}

	private void blinkLine(int number) {
		int move = number%2;
		int column = (number+1) >> 1;

		for (int i = move; i < LevelManager.NUM_ROWS; i++) {
			if (bubblePlay[column][i] != null) {
				bubblePlay[column][i].blink();
			}
		}
	}

	private boolean checkLost() {
		boolean lost = false;

		if (!endOfGame) {
			if (movingBubble != null) {
				if (movingBubble.fixed() && !movingBubble.released() &&
						(movingBubble.getSpritePosition().y >= 380)) {
					lost = true;
				}
			}

			int steps = compressor.getSteps();

			for (int i = 0; i < LevelManager.NUM_COLS; i++) {
				if (bubblePlay[i][(LevelManager.NUM_ROWS - 1) - steps] != null) {
					lost = true;
					break;
				}
			}

			if (lost) {
				penguin.updateState(PenguinSprite.STATE_GAME_LOST);
				if (highscoreManager != null) {
					if (isArcade) {
						highscoreManager.endLevel(nbBubbles);
					}
					else {
						highscoreManager.lostLevel();
					}
				}
				playResult = gameEnum.LOST;
				endOfGame = true;
				initFrozenify();
				soundManager.playSound("lose", R.raw.lose);
			}
		}

		return playResult == gameEnum.LOST;
	}

	public void clampLaunchPosition() {
		if (launchBubblePosition < MIN_LAUNCH_DIRECTION) {
			launchBubblePosition = MIN_LAUNCH_DIRECTION;
		}
		if (launchBubblePosition > MAX_LAUNCH_DIRECTION) {
			launchBubblePosition = MAX_LAUNCH_DIRECTION;
		}
	}

	public void deleteFallingBubble(BubbleSprite sprite) {
		removeSprite(sprite);
		falling.removeElement(sprite);
	}

	/**
	 * Remove the designated goingUp bubble sprite from the vector of
	 * attack bubbles because it is now inserted into the game grid.  The
	 * sprite is not removed from the vector of all sprites in the game
	 * because it has been added to the play field.
	 * @param sprite - the attack bubble inserted into the game grid.
	 */
	public void deleteGoingUpBubble(BubbleSprite sprite) {
		goingUp.removeElement(sprite);
	}

	public void deleteJumpingBubble(BubbleSprite sprite) {
		removeSprite(sprite);
		jumping.removeElement(sprite);
	}

	private void finishFrozenify() {
		if (isArcade) {
			for (int column = 0; column < LevelManager.NUM_COLS; column++) {
				if (scrolling[column] != null) {
					this.spriteToBack(scrolling[column]);
					scrolling[column].frozenify();
				}
			}
		}
		frozenify = false;
		this.addSprite(new ImageSprite(new Rect(152, 190, 337, 116),
				gameLost));
		soundManager.playSound("noh", R.raw.noh);
	}

	private void frozenify() {
		nextFrozenX();
		if (frozenifyX < 0) {
			frozenifyX = LevelManager.NUM_COLS / 2 - 1;
			frozenifyY--;

			if (frozenifyY < 0) {
				finishFrozenify();
				return;
			}
		}

		while ((bubblePlay[frozenifyX][frozenifyY] == null) && (frozenifyY >= 0)) {
			nextFrozenX();
			if (frozenifyX < 0) {
				frozenifyX = LevelManager.NUM_COLS / 2 - 1;
				frozenifyY--;

				if (frozenifyY < 0) {
					finishFrozenify();
					return;
				}
			}
		}

		this.spriteToBack(bubblePlay[frozenifyX][frozenifyY]);
		bubblePlay[frozenifyX][frozenifyY].frozenify();

		this.spriteToBack(launchBubble);
	}

	private void nextFrozenX() {
		if (frozenifyX < LevelManager.NUM_COLS / 2) {
			frozenifyX += 2 * (LevelManager.NUM_COLS / 2 - frozenifyX) - 1;
		} else {
			frozenifyX -= 2 * (frozenifyX - (LevelManager.NUM_COLS / 2 - 1));
		}
	}

	public BubbleSprite[][] getGrid() {
		return bubblePlay;
	}

	public double getMoveDown() {
		return compressor.getMoveDown();
	}

	public boolean getOkToFire() {
		return (movingBubble == null) && (playResult == gameEnum.PLAYING) &&
				(goingUp.size() == 0) && readyToFire;
	}

	private Rect getPenguinRect() {
		return new Rect(361, 436, 361 + PenguinSprite.PENGUIN_WIDTH - 2,
				436 + PenguinSprite.PENGUIN_HEIGHT - 2);
	}

	public double getPosition() {
		return launchBubblePosition;
	}

	public Random getRandom() {
		return random;
	}

	public int getRowOffset() {
		return levelManager.getRowOffset();
	}

	private void initFrozenify() {
		ImageSprite freezeLaunchBubble =
				new ImageSprite(new Rect(301, 389, 34, 42), frozenBubbles[currentColor]);
		ImageSprite freezeNextBubble =
				new ImageSprite(new Rect(301, 439, 34, 42), frozenBubbles[nextColor]);

		this.addSprite(freezeLaunchBubble);
		this.addSprite(freezeNextBubble);

		frozenifyX = LevelManager.NUM_COLS / 2 - 1;
		frozenifyY = LevelManager.NUM_ROWS - 1;
		frozenify  = true;
	}

	/**
	 * Lower the bubbles in play and drop the compressor a step.
	 * @param playSound - <code>true</code> to play the compression sound.
	 */
	public void lowerCompressor(boolean playSound) {
		fixedBubbles = 0;

		if (isArcade) {
			return;
		}

		if (playSound) {
			soundManager.playSound("newroot_solo", R.raw.newroot_solo);
		}

		for (int i = 0; i < LevelManager.NUM_COLS; i++) {
			for (int j = 0; j < (LevelManager.NUM_ROWS - 1); j++) {
				if (bubblePlay[i][j] != null) {
					bubblePlay[i][j].moveDown();

					if ((bubblePlay[i][j].getSpritePosition().y >= 380) && !endOfGame) {
						penguin.updateState(PenguinSprite.STATE_GAME_LOST);
						if (highscoreManager != null)
							highscoreManager.lostLevel();
						playResult = gameEnum.LOST;
						endOfGame = true;
						initFrozenify();
						soundManager.playSound("lose", R.raw.lose);
					}
				}
			}
		}

		compressor.moveDown();
	}

	/**
	 * Move the launched bubble.
	 * @return <code>true</code> if the compressor was lowered.
	 */
	public boolean manageMovingBubble() {
		boolean compressed = false;

		if (movingBubble != null) {
			movingBubble.move();
			if (movingBubble.fixed()) {
				if (!checkLost() && !isArcade) {
					/*
					 * If there are no bubbles in the bubble manager, then the
					 * player has won the game.  The bubble manager counts bubbles
					 * that are fixed in position on the bubble grid.  Thus if
					 * there are attack bubbles in motion when the bubble manager
					 * is cleared, then the attack bubbles will be added to the
					 * bubble manager when they stick to the bubble grid after the
					 * player has already won the game.  This may need to change.
					 */
					if (bubbleManager.countBubbles() == 0) {
						penguin.updateState(PenguinSprite.STATE_GAME_WON);
						this.addSprite(new ImageSprite(new Rect(152, 190,
								152 + 337,
								190 + 116), gameWon));
						if (highscoreManager != null) {
							highscoreManager.endLevel(nbBubbles);
						}
						playResult = gameEnum.WON;
						endOfGame = true;
						soundManager.playSound("applause", R.raw.applause);
					}
					else {
						fixedBubbles++;
						blinkDelay = 0;

						if (fixedBubbles == 8) {
							lowerCompressor(true);
							compressed = true;
						}
					}
				}
				movingBubble = null;
			}
		}
		return compressed;
	}

	public void paint(Canvas c, double scale, int dx, int dy) {
		nextBubble.changeImage(bubbles[nextColor]);
		super     .paint(c, scale, dx, dy);
		compressor.paint(c, scale, dx, dy);
	}

	public void pauseButtonPressed(boolean paused) {
		if (paused) {
			if (pauseButtonSprite != null) {
				this.removeSprite(pauseButtonSprite);
			}
			if (playButtonSprite != null) {
				this.removeSprite(playButtonSprite);
				this.addSprite(playButtonSprite);
			}
		}
		else {
			if (playButtonSprite != null) {
				this.removeSprite(playButtonSprite);
			}
			if (pauseButtonSprite != null) {
				this.removeSprite(pauseButtonSprite);
				this.addSprite(pauseButtonSprite);
			}
		}
	}

	public gameEnum play(boolean key_left, boolean key_right,
						 boolean key_fire, boolean key_swap,
						 double trackball_dx,
						 boolean touch_fire, double touch_x, double touch_y,
						 boolean ats_touch_fire, double ats_touch_dx) {
		int[]   move = new int[2];

		if (touch_fire) {
			key_fire = true;
		}

		if (key_left && !key_right) {
			move[HORIZONTAL_MOVE] = KEY_LEFT;
		}
		else if (key_right && !key_left) {
			move[HORIZONTAL_MOVE] = KEY_RIGHT;
		}
		else {
			move[HORIZONTAL_MOVE] = 0;
		}

		if (key_fire) {
			move[FIRE] = KEY_UP;
		}
		else {
			move[FIRE] = 0;
		}

		if (key_swap) {
			if (!swapPressed) {
				swapNextLaunchBubble();
				swapPressed = true;
			}
		}
		else {
			swapPressed = false;
		}

		if (touch_fire && (movingBubble == null)) {
			double xx = touch_x - 318;
			double yy = 406 - touch_y;
			launchBubblePosition = (Math.PI - Math.atan2(yy, xx)) * 40.0 / Math.PI;
			clampLaunchPosition();
		}

		if ((move[FIRE] == 0) || touch_fire) {
			readyToFire = true;
		}

		if (endOfGame && readyToFire) {
			if (move[FIRE] == KEY_UP) {
				if (playResult == gameEnum.WON) {
					playResult = gameEnum.NEXT_WON;
				}
				else {
					playResult = gameEnum.NEXT_LOST;
				}
				return playResult;
			}
			else {
				penguin.updateState(PenguinSprite.STATE_VOID);

				/*
				 * If the game is over because of bubble overflow, wait until
				 * all the bubbles have stopped moving to freeze them.
				 */
				if (frozenify && (goingUp.size() == 0) && (movingBubble == null)) {
					frozenify();
				}
			}
		}
		else {
			if ((move[FIRE] == KEY_UP) || (hurryTime > HURRY_ME_TIME)) {
				if (getOkToFire()) {
					nbBubbles++;
					movingBubble = new BubbleSprite(new Rect(302, 390, 32, 32),
							launchBubblePosition,
							currentColor,
							bubbles[currentColor],
							frozenBubbles[currentColor],
							targetedBubbles, bubbleBlink,
							bubbleManager, soundManager, this);
					this.addSprite(movingBubble);
					currentColor = nextColor;

					nextColor = bubbleManager.nextBubbleIndex(random);

					nextBubble.changeImage(bubbles[nextColor]);

					launchBubble.changeColor(currentColor);
					penguin.updateState(PenguinSprite.STATE_FIRE);
					soundManager.playSound("launch", R.raw.launch);
					readyToFire = false;
					hurryTime = 0;

					removeSprite(hurrySprite);
				}
				else {
					penguin.updateState(PenguinSprite.STATE_VOID);
				}
			}
			else {
				double dx = 0;
				if (move[HORIZONTAL_MOVE] == KEY_LEFT) {
					dx -= LAUNCH_DIRECTION_STEP;
				}
				if (move[HORIZONTAL_MOVE] == KEY_RIGHT) {
					dx += LAUNCH_DIRECTION_STEP;
				}
				dx += trackball_dx;
				launchBubblePosition += dx;
				clampLaunchPosition();
				launchBubble.changeDirection(launchBubblePosition);
				updatePenguinState(dx);
			}
		}

		/*
		 * The moving bubble is moved twice, which produces smoother
		 * animation. Thus the moving bubble effectively moves at twice the
		 * animation speed with respect to other bubbles that are only
		 * moved once per iteration.
		 */
		manageMovingBubble();
		manageMovingBubble();

		if ((movingBubble == null) && !endOfGame) {
			hurryTime++;
			/*
			 * If hurryTime == 2 (1 + 1) we could be in the "Don't rush me"
			 * mode.  Remove the sprite just in case the user switched
			 * to this mode when the "Hurry" sprite was shown, to make it
			 * disappear.
			 */
			if (hurryTime == 2) {
				removeSprite(hurrySprite);
			}
			if (hurryTime >= 240) {
				if (hurryTime%40 == 10) {
					addSprite(hurrySprite);
					soundManager.playSound("hurry", R.raw.hurry);
				}
				else if (hurryTime%40 == 35) {
					removeSprite(hurrySprite);
				}
			}
		}

		if (!isArcade) {
			if (fixedBubbles == 6) {
				if (blinkDelay < 15) {
					blinkLine(blinkDelay);
				}
				blinkDelay++;
				if (blinkDelay == 40) {
					blinkDelay = 0;
				}
			}
			else if (fixedBubbles == 7) {
				if (blinkDelay < 15) {
					blinkLine(blinkDelay);
				}
				blinkDelay++;
				if (blinkDelay == 25) {
					blinkDelay = 0;
				}
			}
		}

		if (!endOfGame && isArcade) {
			scrollBubbles();
		}

		for (int i = 0; i < falling.size(); i++) {
			((BubbleSprite)falling.elementAt(i)).fall();
		}

		for (int i = 0; i < goingUp.size(); i++) {
			((BubbleSprite)goingUp.elementAt(i)).goUp();
		}

		for (int i = 0; i < jumping.size(); i++) {
			((BubbleSprite)jumping.elementAt(i)).jump();
		}

		/*
		 * Perform game synchronization tasks.
		 */
		if (!endOfGame && movingBubble == null) {
			synchronizeBubbleManager();
		}

		/*
		 * In an arcade or multiplayer game, check if the player lost due to
		 * scrolling or attack bubbles overflowing the play area.
		 */
		if (isArcade) {
			checkLost();
		}

		return gameEnum.PLAYING;
	}

	private Sprite restoreSprite(Bundle map, Vector<BmpWrap> imageList, int i) {
		int left = map.getInt(String.format("%d-left", i));
		int right = map.getInt(String.format("%d-right", i));
		int top = map.getInt(String.format("%d-top", i));
		int bottom = map.getInt(String.format("%d-bottom", i));
		int type = map.getInt(String.format("%d-type", i));
		if (type == Sprite.TYPE_BUBBLE) {
			int color = map.getInt(String.format("%d-color", i));
			double moveX = map.getDouble(String.format("%d-moveX", i));
			double moveY = map.getDouble(String.format("%d-moveY", i));
			double realX = map.getDouble(String.format("%d-realX", i));
			double realY = map.getDouble(String.format("%d-realY", i));
			boolean fixed = map.getBoolean(String.format("%d-fixed", i));
			boolean blink = map.getBoolean(String.format("%d-blink", i));
			boolean released =
					map.getBoolean(String.format("%d-released", i));
			boolean checkJump =
					map.getBoolean(String.format("%d-checkJump", i));
			boolean checkFall =
					map.getBoolean(String.format("%d-checkFall", i));
			int fixedAnim = map.getInt(String.format("%d-fixedAnim", i));
			boolean frozen =
					map.getBoolean(String.format("%d-frozen", i));
			Point lastOpenPosition = new Point(
					map.getInt(String.format("%d-lastOpenPosition.x", i)),
					map.getInt(String.format("%d-lastOpenPosition.y", i)));
			int scroll = map.getInt(String.format("%d-scroll", i));
			int scrollMax = map.getInt(String.format("%d-scrollMax", i));
			return new BubbleSprite(new Rect(left, top, right, bottom),
					color, moveX, moveY, realX, realY,
					fixed, blink, released, checkJump, checkFall,
					fixedAnim, scroll, scrollMax,
					(frozen ? frozenBubbles[color] : bubbles[color]),
					lastOpenPosition,
					frozenBubbles[color],
					targetedBubbles, bubbleBlink,
					bubbleManager, soundManager, this);
		}
		else if (type == Sprite.TYPE_IMAGE) {
			int imageId = map.getInt(String.format("%d-imageId", i));
			return new ImageSprite(new Rect(left, top, right, bottom),
					(BmpWrap)imageList.elementAt(imageId));
		}
		else if (type == Sprite.TYPE_LAUNCH_BUBBLE) {
			int currentColor =
					map.getInt(String.format("%d-currentColor", i));
			double currentDirection =
					map.getDouble(String.format("%d-currentDirection", i));
			return new LaunchBubbleSprite(currentColor, currentDirection,
					launcher, bubbles);
		}
		else if (type == Sprite.TYPE_PENGUIN) {
			int currentPenguin =
					map.getInt(String.format("%d-currentPenguin", i));
			int count = map.getInt(String.format("%d-count", i));
			int finalState =
					map.getInt(String.format("%d-finalState", i));
			int nextPosition =
					map.getInt(String.format("%d-nextPosition", i));

			return new PenguinSprite(getPenguinRect(), penguins, random,
					currentPenguin, count, finalState,
					nextPosition);
		}
		else {
			Log.e("frozen-bubble", "Unrecognized sprite type: " + type);
			return null;
		}
	}

	public void restoreState(Bundle map, Vector<BmpWrap> imageList) {
		Vector<Sprite> savedSprites = new Vector<Sprite>();
		int numSavedSprites =
				map.getInt("numSavedSprites");
		for (int i = 0; i < numSavedSprites; i++) {
			savedSprites.addElement(restoreSprite(map, imageList, i));
		}

		restoreSprites(map, savedSprites);

		if (jumping == null) {
			jumping = new Vector<Sprite>();
		}
		int numJumpingSprites =
				map.getInt("numJumpingSprites");
		for (int i = 0; i < numJumpingSprites; i++) {
			int spriteIdx = map.getInt(String.format("jumping-%d", i));
			jumping.addElement(savedSprites.elementAt(spriteIdx));
		}
		if (goingUp == null) {
			goingUp = new Vector<Sprite>();
		}
		int numGoingUpSprites =
				map.getInt("numGoingUpSprites");
		for (int i = 0; i < numGoingUpSprites; i++) {
			int spriteIdx = map.getInt(String.format("goingUp-%d", i));
			goingUp.addElement(savedSprites.elementAt(spriteIdx));
		}
		if (falling == null) {
			falling = new Vector<Sprite>();
		}
		int numFallingSprites =
				map.getInt("numFallingSprites");
		for (int i = 0; i < numFallingSprites; i++) {
			int spriteIdx = map.getInt(String.format("falling-%d", i));
			falling.addElement(savedSprites.elementAt(spriteIdx));
		}
		if (bubblePlay == null) {
			bubblePlay = new BubbleSprite[LevelManager.NUM_COLS]
					[LevelManager.NUM_ROWS];
		}
		for (int i = 0; i < LevelManager.NUM_COLS; i++) {
			for (int j = 0; j < LevelManager.NUM_ROWS; j++) {
				int spriteIdx =
						map.getInt(String.format("play-%d-%d", i, j));
				if (spriteIdx != -1) {
					bubblePlay[i][j] = (BubbleSprite)savedSprites.elementAt(spriteIdx);
				}
				else {
					bubblePlay[i][j] = null;
				}
			}
		}
		if (isArcade) {
			if (scrolling == null) {
				scrolling = new BubbleSprite[LevelManager.NUM_COLS];
			}
			for (int i = 0; i < LevelManager.NUM_COLS; i++) {
				int spriteIdx =
						map.getInt(String.format("scrolling-%d", i));
				if (spriteIdx != -1) {
					scrolling[i] = (BubbleSprite)savedSprites.elementAt(spriteIdx);
				}
				else {
					scrolling[i] = null;
				}
			}
		}
		int launchBubbleId =
				map.getInt("launchBubbleId");
		launchBubble = (LaunchBubbleSprite)savedSprites.elementAt(launchBubbleId);
		launchBubblePosition =
				map.getDouble("launchBubblePosition");
		int penguinId = map.getInt("penguinId");
		penguin       = (PenguinSprite)savedSprites.elementAt(penguinId);
		compressor.restoreState(map);
		int nextBubbleId = map.getInt("nextBubbleId");
		nextBubble       = (ImageSprite)savedSprites.elementAt(nextBubbleId);
		currentColor     = map.getInt("currentColor");
		nextColor        = map.getInt("nextColor");
		int movingBubbleId =
				map.getInt("movingBubbleId");
		if (movingBubbleId == -1) {
			movingBubble = null;
		}
		else {
			movingBubble = (BubbleSprite)savedSprites.elementAt(movingBubbleId);
		}
		bubbleManager.restoreState(map);
		int pauseButtonId =
				map.getInt("pauseButtonId");
		if (pauseButtonId < 1) {
			pauseButtonSprite = null;
		}
		else {
			pauseButtonSprite = (ImageSprite) savedSprites.elementAt(pauseButtonId);
		}
		int playButtonId =
				map.getInt("playButtonId");
		if (playButtonId < 1) {
			playButtonSprite = null;
		}
		else {
			playButtonSprite = (ImageSprite) savedSprites.elementAt(playButtonId);
		}
		fixedBubbles   = map.getInt("fixedBubbles");
		nbBubbles      = map.getInt("nbBubbles");
		blinkDelay     = map.getInt("blinkDelay");
		int hurryId    = map.getInt("hurryId");
		hurrySprite    = (ImageSprite)savedSprites.elementAt(hurryId);
		hurryTime      = map.getInt("hurryTime");
		readyToFire    = map.getBoolean("readyToFire");
		endOfGame      = map.getBoolean("endOfGame");
		frozenify      = map.getBoolean("frozenify");
		frozenifyX     = map.getInt("frozenifyX");
		frozenifyY     = map.getInt("frozenifyY");
	}

	public void saveState(Bundle map) {
		Vector<Sprite> savedSprites = new Vector<Sprite>();

		saveSprites(map, savedSprites);

		for (int i = 0; i < jumping.size(); i++) {
			((Sprite)jumping.elementAt(i)).saveState(map, savedSprites);
			map.putInt(String.format("jumping-%d", i),
					((Sprite)jumping.elementAt(i)).getSavedId());
		}
		map.putInt("numJumpingSprites", jumping.size());
		for (int i = 0; i < goingUp.size(); i++) {
			((Sprite)goingUp.elementAt(i)).saveState(map, savedSprites);
			map.putInt(String.format("goingUp-%d", i),
					((Sprite)goingUp.elementAt(i)).getSavedId());
		}
		map.putInt("numGoingUpSprites", goingUp.size());
		for (int i = 0; i < falling.size(); i++) {
			((Sprite)falling.elementAt(i)).saveState(map, savedSprites);
			map.putInt(String.format("falling-%d", i),
					((Sprite)falling.elementAt(i)).getSavedId());
		}
		map.putInt("numFallingSprites", falling.size());
		for (int i = 0; i < LevelManager.NUM_COLS; i++) {
			for (int j = 0; j < LevelManager.NUM_ROWS; j++) {
				if (bubblePlay[i][j] != null) {
					bubblePlay[i][j].saveState(map, savedSprites);
					map.putInt(String.format("play-%d-%d", i, j),
							bubblePlay[i][j].getSavedId());
				}
				else {
					map.putInt(String.format("play-%d-%d", i, j), -1);
				}
			}
		}
		if (isArcade) {
			for (int i = 0; i < LevelManager.NUM_COLS; i++) {
				if (scrolling[i] != null) {
					scrolling[i].saveState(map, savedSprites);
					map.putInt(String.format("scrolling-%d", i),
							scrolling[i].getSavedId());
				}
				else {
					map.putInt(String.format("scrolling-%d", i), -1);
				}
			}
		}
		launchBubble.saveState(map, savedSprites);
		map.putInt("launchBubbleId",
				launchBubble.getSavedId());
		map.putDouble("launchBubblePosition",
				launchBubblePosition);
		penguin.saveState(map, savedSprites);
		compressor.saveState(map);
		map.putInt("penguinId", penguin.getSavedId());
		nextBubble.saveState(map, savedSprites);
		map.putInt("nextBubbleId",
				nextBubble.getSavedId());
		map.putInt("currentColor", currentColor);
		map.putInt("nextColor", nextColor);
		if (movingBubble != null) {
			movingBubble.saveState(map, savedSprites);
			map.putInt("movingBubbleId",
					movingBubble.getSavedId());
		}
		else {
			map.putInt("movingBubbleId", -1);
		}
		bubbleManager.saveState(map);
		if (pauseButtonSprite != null) {
			pauseButtonSprite.saveState(map, savedSprites);
			map.putInt("pauseButtonId",
					pauseButtonSprite.getSavedId());
		}
		else {
			map.putInt("pauseButtonId", -1);
		}
		if (playButtonSprite != null) {
			playButtonSprite.saveState(map, savedSprites);
			map.putInt("playButtonId",
					playButtonSprite.getSavedId());
		}
		else {
			map.putInt("playButtonId", -1);
		}
		map.putInt("fixedBubbles", fixedBubbles);
		map.putInt("nbBubbles", nbBubbles);
		map.putInt("blinkDelay", blinkDelay);
		hurrySprite.saveState(map, savedSprites);
		map.putInt("hurryId", hurrySprite.getSavedId());
		map.putInt("hurryTime", hurryTime);
		map.putBoolean("readyToFire", readyToFire);
		map.putBoolean("endOfGame", endOfGame);
		map.putBoolean("frozenify", frozenify);
		map.putInt("frozenifyX", frozenifyX);
		map.putInt("frozenifyY", frozenifyY);
		map.putInt("numSavedSprites",
				savedSprites.size());
		for (int i = 0; i < savedSprites.size(); i++) {
			((Sprite)savedSprites.elementAt(i)).clearSavedId();
		}
	}

	void scrollBubbles() {
		boolean scroll = compressor.checkScroll();
		int moveDown = (int) compressor.getMoveDown();
		if (scroll) {
			for (int row = LevelManager.NUM_ROWS - 1; row >= 0; row--) {
				for (int column = 0; column < LevelManager.NUM_COLS; column++) {
					if (bubblePlay[column][row] != null) {
						bubblePlay[column][row].scroll(moveDown);
					}
				}
			}
			for (int column = 0; column < LevelManager.NUM_COLS; column++) {
				if (scrolling[column] != null) {
					scrolling[column].scroll(moveDown);
				}
			}
		}
		if ((movingBubble == null) && (moveDown >= 28.)) {
			compressor.moveDownSubtract(28.);
			for (int row = LevelManager.NUM_ROWS - 1; row > 0; row--) {
				for (int column = 0; column < LevelManager.NUM_COLS; column++) {
					bubblePlay[column][row    ] = bubblePlay[column][row - 1];
					bubblePlay[column][row - 1] = null;
				}
			}
			for (int column = 0; column < LevelManager.NUM_COLS; column++) {
				bubblePlay[column][0] = scrolling[column];
			}
			addScrollRow();
		}
	}

	public void setPosition(double value) {
		if (!endOfGame) {
			double dx = value - launchBubblePosition;
			/*
			 * For small position changes, don't update the penguin state.
			 */
			if ((dx <  LAUNCH_DIRECTION_MIN_STEP) &&
					(dx > -LAUNCH_DIRECTION_MIN_STEP))
				dx = 0;
			launchBubblePosition = value;
			clampLaunchPosition();
			launchBubble.changeDirection(launchBubblePosition);
			updatePenguinState(dx);
		}
	}

	public void swapNextLaunchBubble() {
		if (currentColor != nextColor) {
			int tempColor = currentColor;
			currentColor  = nextColor;
			nextColor     = tempColor;
			launchBubble.changeColor(currentColor);
			nextBubble.changeImage(bubbles[nextColor]);
			soundManager.playSound("whip", R.raw.whip);
		}
	}

	/**
	 * This function is an unfortunate patch that is necessitated due to
	 * the fact that there is as of yet an unfixed bug in the BubbleSprite
	 * management code.
	 * <p>Somewhere amongst goUp() and move() in BubbleSprite.java, a flaw
	 * exists whereby a bubble is added to the bubble manager, and the
	 * bubble sprite is added to the game screen, but the entry in the
	 * bubblePlay grid was either rendered null or a bubble superposition
	 * in the grid occurred.  The former is suspected, because ensuring
	 * the grid location is null before assigning a bubble sprite to it is
	 * very rigorously enforced.
	 * <p><b>TODO</b> - fix the grid entry bug.
	 */
	public void synchronizeBubbleManager() {
		int numBubblesManager = bubbleManager.countBubbles();
		int numBubblesPlay = 0;
		/*
		 * Check the bubble sprite grid for occupied locations.
		 */
		for (int i = 0; i < LevelManager.NUM_COLS; i++) {
			for (int j = 0; j < LevelManager.NUM_ROWS; j++) {
				if (bubblePlay[i][j] != null ) {
					numBubblesPlay++;
				}
			}
		}
		if (isArcade) {
			for (int i = 0; i < LevelManager.NUM_COLS; i++) {
				if (scrolling[i] != null) {
					numBubblesPlay++;
				}
			}
		}
		/*
		 * If the number of bubble sprite grid entries does not match the
		 * number of bubbles in the bubble manager, then we need to re-
		 * initialize the bubble manager, and re-initialize all the bubble
		 * sprites on the game screen.  You would be unable to win prior to
		 * the addition of this synchronization code due to the number of
		 * bubbles in the bubble manager never reaching zero, and the excess
		 * sprite or sprites would remain stuck on the screen.
		 */
		if (numBubblesManager != numBubblesPlay) {
			bubbleManager.initialize();
			removeAllBubbleSprites();
			for (int i = 0; i < LevelManager.NUM_COLS; i++) {
				for (int j = 0; j < LevelManager.NUM_ROWS; j++) {
					if (bubblePlay[i][j] != null ) {
						bubblePlay[i][j].addToManager();
						this.addSprite(bubblePlay[i][j]);
					}
				}
			}
			if (isArcade) {
				for (int i = 0; i < LevelManager.NUM_COLS; i++) {
					if (scrolling[i] != null) {
						scrolling[i].addToManager();
						this.addSprite(scrolling[i]);
					}
				}
			}
			for (int i = 0; i < falling.size(); i++) {
				this.addSprite(falling.elementAt(i));
			}
			for (int i = 0; i < goingUp.size(); i++) {
				this.addSprite(goingUp.elementAt(i));
			}
			for (int i = 0; i < jumping.size(); i++) {
				this.addSprite(jumping.elementAt(i));
			}
		}
	}

	public void updatePenguinState(double dx) {
		if (dx < 0) {
			penguin.updateState(PenguinSprite.STATE_TURN_LEFT);
		}
		else if (dx > 0) {
			penguin.updateState(PenguinSprite.STATE_TURN_RIGHT);
		}
		else {
			penguin.updateState(PenguinSprite.STATE_VOID);
		}
	}
}
