package com.teknologika;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.view.ContextThemeWrapper;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getnimapp.frozenbubble.R;

/**
 * A simple {@link DialogFragment} subclass.
 */
public class CustomDialog extends DialogFragment implements View.OnClickListener {
	public static final String TAG = CustomDialog.class.getSimpleName();

	public interface OnOptionSelectedListener {
		void onOptionSelectedListener(CustomDialog fragment, int option, Button button);
	}

	private static final String ARG_TITLE = "title";
	private static final String ARG_IMAGE = "image";
	private static final String ARG_OPTIONS = "options";

	private int mTitle;
	private int mImage;
	private String [] mOptions;

	private CustomDialog.OnOptionSelectedListener mListener;

	public CustomDialog() {
		// Required empty public constructor
	}

	public static CustomDialog newInstance(int title, int image, String [] options, OnOptionSelectedListener listener) {
		CustomDialog fragment = new CustomDialog();
		fragment.setListener(listener);
		Bundle args = new Bundle();
		args.putInt(ARG_TITLE, title);
		args.putInt(ARG_IMAGE, image);
		args.putStringArray(ARG_OPTIONS, options);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mTitle = getArguments().getInt(ARG_TITLE, 0);
			mImage = getArguments().getInt(ARG_IMAGE, 0);
			mOptions = getArguments().getStringArray(ARG_OPTIONS);
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getActivity().getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(
				i -> {
					getActivity().getWindow().getDecorView().setSystemUiVisibility(
							View.SYSTEM_UI_FLAG_LAYOUT_STABLE
									| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
									| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
									| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
									| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
									| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
				});
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// Set the IMMERSIVE flag.
		// Set the content to appear under the system bars so that the content
		// doesn't resize when the system bars hide and show.
		dialog.getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
						| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
						| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		return dialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_custom_dialog, container, false);

		if (mTitle != 0) {
			((TextView)view.findViewById(R.id.title)).setText(mTitle);
		} else  {
			view.findViewById(R.id.title).setVisibility(View.GONE);
		}

		if (mImage != 0) {
			((ImageView)view.findViewById(R.id.image)).setImageResource(mImage);
		} else {
			view.findViewById(R.id.image).setVisibility(View.GONE);
		}

		if (mOptions != null) {
			LinearLayout buttonsLayout = view.findViewById(R.id.buttons);

			for (int i = 0; i < mOptions.length; i++) {
				String option = mOptions[i];
				Button button = new Button(new ContextThemeWrapper(getActivity(), R.style.HomeScreenButton));
				button.setText(option);
				button.setBackgroundResource(R.drawable.btn);
				Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/indigojo.ttf");
				button.setTypeface(face);
				button.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.gui_text_size));
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT, //getResources().getDimensionPixelSize(R.dimen.home_button_width),
						LinearLayout.LayoutParams.WRAP_CONTENT
				);
				params.gravity = Gravity.CENTER_HORIZONTAL;
				params.setMargins(
						getResources().getDimensionPixelSize(R.dimen.home_button_margin),
						getResources().getDimensionPixelSize(R.dimen.home_button_margin),
						getResources().getDimensionPixelSize(R.dimen.home_button_margin),
						getResources().getDimensionPixelSize(R.dimen.home_button_margin)
				);
				button.setLayoutParams(params);
				button.setOnClickListener(this);
				button.setTag(i);
				buttonsLayout.addView(button);
			}
		}

		return view;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onClick(View view) {
		if (mListener != null) {
			mListener.onOptionSelectedListener(this, (int)view.getTag(), (Button)view);
		}
	}

	@Override public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		if (mListener != null) {
			mListener.onOptionSelectedListener(this, -1, null);
		}
	}

	private void setListener(OnOptionSelectedListener listener) {
		mListener = listener;
	}
}
