package com.teknologika;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;

import com.efortin.frozenbubble.MainActivity;
import com.efortin.frozenbubble.ModPlayer;
import com.efortin.frozenbubble.Preferences;
import com.efortin.frozenbubble.PreferencesHelper;
import com.getnimapp.frozenbubble.R;
import com.peculiargames.andmodplug.PlayerThread;

import org.jfedor.frozenbubble.SoundManager;

public class HomeFragment extends Fragment {

	public interface OnStartFrozenBubbleListener {
		void onStartFrozenBubbleListener(boolean arcade);
	}

	private OnStartFrozenBubbleListener mListener;

	public ModPlayer      myModPlayer   = null;
	public Preferences myPreferences = null;
	private SoundManager   mSoundManager = null;

	public HomeFragment() {
		// Required empty public constructor
	}

	public static HomeFragment newInstance() {
		HomeFragment fragment = new HomeFragment();
		Bundle args = new Bundle();
		//args.putString(ARG_PARAM1, param1);
		//args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			//mParam1 = getArguments().getString(ARG_PARAM1);
			//mParam2 = getArguments().getString(ARG_PARAM2);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_home, container, false);
		restoreGamePrefs();
		startHomeScreen(view);
		return view;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnStartFrozenBubbleListener) {
			mListener = (OnStartFrozenBubbleListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		cleanUp();
	}

	private void cleanUp() {
		if (mSoundManager != null) {
			mSoundManager.cleanUp();
		}
		mSoundManager = null;
		if (myModPlayer != null) {
			myModPlayer.destroyMusicPlayer();
		}
		myModPlayer = null;
	}

	public void restoreGamePrefs() {
		SharedPreferences sp =
				PreferenceManager.getDefaultSharedPreferences(getActivity());
		myPreferences = PreferencesHelper.getDefaultPrefs(sp);
	}

	/**
	 * Start the game with the specified game mode.
	 * @param arcadeGame - endless arcade game that scrolls new bubbles.
	 */
	private void startFrozenBubble(boolean arcadeGame) {
		if (mListener != null) {
			mListener.onStartFrozenBubbleListener(arcadeGame);
		}
	}

	@SuppressLint("ClickableViewAccessibility")
	public void startHomeScreen(View view) {
		/*
		 * Listeners for the play arcade button.
		 */
		Button playArcadeButton = view.findViewById(R.id.btn_arcade);
		playArcadeButton.setOnClickListener(v -> {
			mSoundManager.playSound("stick", R.raw.stick);
			/*
			 * Process the button tap and load the saved game.
			 */
			startFrozenBubble(true);
		});

		/*
		 * Listeners for the play puzzle button.
		 */
		Button playPuzzleButton = view.findViewById(R.id.btn_puzzle);
		playPuzzleButton.setOnClickListener(v -> {
			mSoundManager.playSound("stick", R.raw.stick);
			/*
			 * Process the button tap and load the saved game.
			 */
			startFrozenBubble(false);
		});

		/*
		 * Create a sound manager to play sound effects.
		 */
		mSoundManager = new SoundManager(getActivity());

		/*
		 * Load game sound effects.
		 */
		mSoundManager.loadSound("stick", R.raw.stick );

		/*
		 * Create a new music player to play the home screen music.
		 */
		myModPlayer =
				new ModPlayer(getActivity(),
						R.raw.introzik,
						PlayerThread.LOOP_SONG_FOREVER,
						myPreferences.musicOn,
						false);
	}
}
