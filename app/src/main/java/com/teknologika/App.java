package com.teknologika;

import android.app.Application;

import com.getnimapp.frozenbubble.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				//.setDefaultFontPath("fonts/indigojo.ttf")
				.setFontAttrId(R.attr.fontPath)
				.build()
		);
	}
}
