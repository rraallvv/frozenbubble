package com.teknologika;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.efortin.frozenbubble.MainActivity;
import com.efortin.frozenbubble.ModPlayer;
import com.efortin.frozenbubble.Preferences;
import com.efortin.frozenbubble.PreferencesHelper;
import com.getnimapp.frozenbubble.R;
import com.peculiargames.andmodplug.PlayerThread;
import com.teknologika.getnim.adnim.AdsHelper;

import org.jfedor.frozenbubble.FrozenGame;
import org.jfedor.frozenbubble.GameScreen;
import org.jfedor.frozenbubble.GameView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import static org.jfedor.frozenbubble.FrozenGame.PREFS_NAME;

public class GameFragment extends Fragment {

	public interface OnGoToHomeScreenListener {
		void onGoToHomeScreenListener();
	}

	private OnGoToHomeScreenListener mListener;

	private static Preferences prefs = new Preferences();

	public final static String SAVE_GAME    = "FrozenBubble.save";

	public boolean    allowUnpause          = true;
	public GameView   mGameView             = null;
	public ModPlayer  myModPlayer           = null;
	public GameView.GameThread mGameThread           = null;

	private final int[] MODlist = {
			R.raw.ambientpower,
			R.raw.ambientlight,
			R.raw.androidrupture,
			R.raw.artificial,
			R.raw.aftertherain,
			R.raw.bluestars,
			R.raw.chungababe,
			R.raw.crystalhammer,
			R.raw.dreamscope,
			R.raw.freefall,
			R.raw.gaeasawakening,
			R.raw.homesick,
			R.raw.ifcrystals,
			R.raw.popcorn,
			R.raw.stardustmemories,
			R.raw.sunshineofthemorningsun,
			R.raw.technostyleiii
	};

	public GameFragment() {
		// Required empty public constructor
	}

	public static GameFragment newInstance(Boolean arcade) {
		GameFragment fragment = new GameFragment();
		Bundle args = new Bundle();
		args.putBoolean("arcadeGame", arcade);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		boolean arcade = false;
		if (getArguments() != null) {
			arcade = getArguments().getBoolean("arcadeGame");
		}
		startGame(arcade, savedInstanceState);
		return mGameView;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnGoToHomeScreenListener) {
			mListener = (OnGoToHomeScreenListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	/**
	 * Invoked when the Activity is finishing or being destroyed by the
	 * system.
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		cleanUp();
	}

	/*
	 * The following methods are used to retrieve and set the volatile
	 * memory game options variables.  They are all static, so are
	 * available to all activities within this application while the
	 * application is running.
	 */

	public static int getDifficulty() {
		return prefs.difficulty;
	}

	public static void setDifficulty(int newDifficulty) {
		prefs.difficulty = newDifficulty;
	}

	public static boolean getMusicOn() {
		return prefs.musicOn;
	}

	public static void setMusicOn(boolean mo) {
		prefs.musicOn = mo;
	}

	public static boolean getSoundOn() {
		return prefs.soundOn;
	}

	public static void setSoundOn(boolean so) {
		prefs.soundOn = so;
	}

	/*
	 * Following are general utility functions.
	 */

	/**
	 * Perform activity cleanup.  <b>This must only be called when the
	 * activity is being destroyed.</b>
	 */
	public void cleanUp() {
		/*
		 * The current game is being destroyed, so reset the static game
		 * state variables.
		 */
		FrozenGame.arcadeGame = false;

		cleanUpGameView();

		if (myModPlayer != null) {
			myModPlayer.destroyMusicPlayer();
		}
		myModPlayer = null;
	}

	private void cleanUpGameView() {
		mGameThread = null;
		if (mGameView != null) {
			mGameView.cleanUp();
		}
		mGameView = null;
	}

	public void exit() {
		/*
		 * Preserve game information and perform activity cleanup.
		 */
		pause();
		if (mGameThread != null)
			mGameThread.setRunning(false);
		cleanUp();
		/*
		 * Clear the flag indicating that the system passed us a bundle to
		 * save the game state.
		 */
		SharedPreferences.Editor editor =
				PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
		editor.putBoolean("systemSave", false);
		editor.commit();

		if (mListener != null) {
			mListener.onGoToHomeScreenListener();
		}
	}

	public void pauseGameDialog() {
		CustomDialog fragment = CustomDialog.newInstance(
				R.string.pause,
				R.drawable.pause_panel_player,
				new String[]{"[", "\\", "=", getSoundOn() ? "{" : "|", getMusicOn() ? "}" : "~"},
				(fr, option, button) -> {
					boolean prefsUpdated = false;

					switch (option) {
						case 0:
							/*
							 * User clicked Exit.  Save and quit.  Only save if this isn't a
							 * custom game.
							 */
							exit();
							fr.dismiss();
							break;

						case 1:
							/*
							 * User clicked Resume.  Continue the current game.
							 */
							resume();
							fr.dismiss();
							break;

						case 2:
							/*
							 * User clicked Restart.  Restart the game.
							 */
							newGameDialog();
							fr.dismiss();
							break;

						case 3: {
							boolean isOn = getSoundOn();
							button.setText(!isOn ? "{" : "|");
							setSoundOn(!isOn);
							prefsUpdated = true;
						}
						break;

						case 4: {
							boolean isOn = getMusicOn();
							button.setText(!isOn ? "}" : "~");
							setMusicOn(!isOn);
							if (myModPlayer != null) {
								myModPlayer.setMusicOn(!isOn);
							}
							prefsUpdated = true;
						}
						break;

						default:
							resume();
					}

					if (prefsUpdated) {
						SharedPreferences sp =
								PreferenceManager.getDefaultSharedPreferences(getActivity().getApplication());
						PreferencesHelper.saveDefaultPreferences(prefs, sp);
					}
				}
		);
		fragment.show(getActivity().getSupportFragmentManager(), CustomDialog.TAG);
	}

	/**
	 * Restore the game options from the saved preferences, and register
	 * the device orientation listener to detect orientation changes.
	 */
	private void initGameOptions() {
		restoreGamePrefs();
	}

	/**
	 * Start a new game and music player.
	 */
	public void newGame() {
		if (mGameThread != null) {
			mGameThread.newGame(true);
		}

		playMusic(false);
	}

	private void newGameDialog() {
		CustomDialog fragment = CustomDialog.newInstance(
				R.string.menu_new_game,
				0,
				new String[]{"^", "_"},
				(fr, option, button) -> {
					switch (option) {
						case 0:
							newGame();
							break;

						case 1:
							pauseGameDialog();
							break;
					}
					fr.dismiss();
				}
		);
		fragment.show(getActivity().getSupportFragmentManager(), CustomDialog.TAG);
	}

	private void newGameView(Bundle map) {
		if (map != null) {
			/*
			 * Force the game locale to be confined to the local device when
			 * restoring a saved game.  Restoring a network game is of little
			 * to no value as opposed to simply starting a new game.
			 */
			FrozenGame.arcadeGame = map.getBoolean("arcadeGame", false               );
		}
		mGameView = new GameView(getActivity(), FrozenGame.arcadeGame);

		mGameView.setGameListener((MainActivity)getActivity());
		mGameThread = mGameView.getThread();
		mGameThread.restoreState(map);
		mGameView.requestFocus();
	}

	/**
	 * Pause the game.
	 */
	public void pause() {
		if (mGameThread != null)
			mGameThread.pause();

		/*
		 * Pause the MOD player.
		 */
		if (myModPlayer != null)
			myModPlayer.pausePlay();
	}

	private void resume() {
		if (mGameThread != null) {
			allowUnpause = true;
			mGameThread.resumeGame();
			mGameThread.setState(GameScreen.stateEnum.RUNNING);
			mGameThread.mHighScoreManager.resumeLevel();
		}
		if (mGameView != null) {
			mGameView.levelStarted = true;
			if (mGameView.mGameListener != null) {
				mGameView.mGameListener.onGameEvent(GameScreen.eventEnum.GAME_RESUME);
			}
		}
	}

	/**
	 * This function determines whether a music player instance needs to
	 * be created or if one already exists.  Then, based on the current
	 * level, the song to play is calculated and loaded.  If desired, the
	 * song will start playing immediately, or it can remain paused.
	 * @param startPlaying - If <code>true</code>, the song starts playing
	 * immediately.  Otherwise it is paused and must be unpaused to start
	 * playing.
	 */
	public void playMusic(boolean startPlaying)
	{
		int loopCount;
		int modNow;
		/*
		 * Ascertain which song to play.  For a single player game, the song
		 * is based on the current level.  For an arcade game, a two player
		 * game, or if the game thread has been destroyed, the song is
		 * selected at random.
		 */
		if (songsSequential()) {
			loopCount = PlayerThread.LOOP_SONG_FOREVER;
			modNow    = mGameThread.getCurrentLevelIndex() % MODlist.length;
		}
		else
		{
			loopCount = PlayerThread.LOOP_SONG_FOREVER;
			modNow = new Random().nextInt(MODlist.length);
		}
		/*
		 * Determine whether to create a music player or load the song.
		 */
		if (myModPlayer == null) {
			myModPlayer = new ModPlayer(getActivity(),
					MODlist[modNow],
					loopCount,
					getMusicOn(),
					!startPlaying);
			myModPlayer.setPlayerListener((MainActivity)getActivity());
		}
		else {
			myModPlayer.loadNewSong(MODlist[modNow], startPlaying);
		}
		allowUnpause = true;
	}

	/**
	 * Restore the game from a player saved bundle.
	 * <p>Note that this implementation uses a practice that is not
	 * recommended by the API documentation, as serializing a
	 * <code>Parcel</code> object is not guaranteed to be compatible
	 * amongst disparate versions of the Android OS.
	 */
	private Bundle restoreSavedGame() {
		Bundle inState = null;
		Parcel parcel  = Parcel.obtain();

		try {
			FileInputStream fis = getActivity().openFileInput((FrozenGame.arcadeGame ? "Arcade" : "Puzzle") + SAVE_GAME);
			byte[] array = new byte[(int) fis.getChannel().size()];
			fis.read(array, 0, array.length);
			fis.close();
			parcel.unmarshall(array, 0, array.length);
			parcel.setDataPosition(0);
			inState = parcel.readBundle();
			inState.putAll(inState);
		} catch (FileNotFoundException fnfe) {
			/*
			 * Cannot open file, so the game could not be restored.
			 */
		} catch (IOException ioe) {
			/*
			 * Error occurred while reading from file.
			 */
			ioe.printStackTrace();
		} finally {
			parcel.recycle();
		}

		return inState;
	}

	/**
	 * Load the game options from the saved shared preferences.
	 */
	private void restoreGamePrefs() {
		SharedPreferences sp =
				PreferenceManager.getDefaultSharedPreferences(getActivity());
		prefs = PreferencesHelper.getDefaultPrefs(sp);
	}

	/**
	 * Save the game to a player saved bundle.
	 * <p>Note that this implementation uses a practice that is not
	 * recommended by the API documentation, as serializing a
	 * <code>Parcel</code> object is not guaranteed to be compatible
	 * amongst disparate versions of the Android OS.
	 */
	public void saveGame(Bundle map) {
		Bundle outState = map;
		Parcel parcel   = Parcel.obtain();

		if (outState == null) {
			outState = new Bundle();
			saveState(outState);
		}

		try {
			FileOutputStream fos = getActivity().openFileOutput((FrozenGame.arcadeGame ? "Arcade" : "Puzzle") +  SAVE_GAME, Context.MODE_PRIVATE);
			outState.writeToParcel(parcel, 0);
			fos.write(parcel.marshall());
			fos.flush();
			fos.close();
		} catch (FileNotFoundException fnfe) {
			/*
			 * Cannot create file, so the game could not be saved.
			 */
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			/*
			 * Error occurred while writing to file.
			 */
			ioe.printStackTrace();
		} finally {
			parcel.recycle();
		}
	}

	/**
	 * Save critically important game information.
	 */
	public void saveState(Bundle map) {
		if (!FrozenGame.arcadeGame && (mGameThread != null)) {
			/*
			 * Allow level editor functionalities.
			 */
			SharedPreferences sp = getActivity().getSharedPreferences(PREFS_NAME,
					Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sp.edit();
			/*
			 * If the game wasn't launched from the level editor, save the
			 * last level played.
			 */
			editor.putInt("level", mGameThread.getCurrentLevelIndex());
			editor.commit();
		}
		if (map != null) {
			map.putBoolean("arcadeGame", FrozenGame.arcadeGame);

			if (mGameThread != null) {
				mGameThread.saveState(map);
			}
		}
	}

	/**
	 * For a Puzzle game, there are consecutive levels.  Each level will
	 * have a corresponding song.
	 * @return <code>true</code> if the song index should be selected
	 * based on the current game level.
	 */
	public boolean songsSequential() {
		return(!FrozenGame.arcadeGame && (mGameThread != null));
	}

	/**
	 * Method to start a game using default levels, if single player game
	 * mode was selected.
	 * <p>This method is also used to start a multiplayer game.
	 * @param arcade - The type of game to start.
	 * @param savedInstanceState - the bundle of saved state information.
	 */
	private void startGame(boolean arcade, Bundle savedInstanceState) {
		/*
		 * Initialize the game state variables to safe defaults.
		 */
		FrozenGame.arcadeGame = arcade;

		initGameOptions();

		newGameView(restoreSavedGame());

		if (mGameView != null)
			mGameView.requestLayout();

		playMusic(false);
	}
}
