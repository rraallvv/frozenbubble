/*
 *                 [[ Frozen-Bubble ]]
 *
 * Copyright (c) 2000-2003 Guillaume Cottenceau.
 * Java sourcecode - Copyright (c) 2003 Glenn Sanson.
 * Additional source - Copyright (c) 2013 Eric Fortin.
 *
 * This code is distributed under the GNU General Public License
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 or 3, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 * Free Software Foundation, Inc.
 * 675 Mass Ave
 * Cambridge, MA 02139, USA
 *
 * Artwork:
 *    Alexis Younes <73lab at free.fr>
 *      (everything but the bubbles)
 *    Amaury Amblard-Ladurantie <amaury at linuxfr.org>
 *      (the bubbles)
 *
 * Soundtrack:
 *    Matthias Le Bidan <matthias.le_bidan at caramail.com>
 *      (the three musics and all the sound effects)
 *
 * Design & Programming:
 *    Guillaume Cottenceau <guillaume.cottenceau at free.fr>
 *      (design and manage the project, whole Perl sourcecode)
 *
 * Java version:
 *    Glenn Sanson <glenn.sanson at free.fr>
 *      (whole Java sourcecode, including JIGA classes
 *             http://glenn.sanson.free.fr/jiga/)
 *
 * Android port:
 *    Pawel Aleksander Fedorynski <pfedor@fuw.edu.pl>
 *    Eric Fortin <videogameboy76 at yahoo.com>
 *    Copyright (c) Google Inc.
 *
 *          [[ http://glenn.sanson.free.fr/fb/ ]]
 *          [[ http://www.frozen-bubble.org/   ]]
 */

package com.efortin.frozenbubble;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.getnimapp.frozenbubble.R;
import com.peculiargames.andmodplug.PlayerThread;
import com.teknologika.GameFragment;
import com.teknologika.HomeFragment;
import com.teknologika.getnim.adnim.AdsHelper;

import org.jfedor.frozenbubble.FrozenGame;
import org.jfedor.frozenbubble.GameScreen;
import org.jfedor.frozenbubble.GameView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.teknologika.getnim.adnim.AdsHelper.EARN_UNITS;
import static com.teknologika.getnim.adnim.AdsHelper.LUNAS_PER_NIM;

public class MainActivity extends AppCompatActivity
		implements GameView.GameListener,
		PlayerThread.PlayerListener,
		HomeFragment.OnStartFrozenBubbleListener,
		GameFragment.OnGoToHomeScreenListener {

	private Fragment mFrament = null;

	private static final int INTERSTITIAL_AD_FREQUENCY = 4;
	private static int mInterstitialAdCounter = INTERSTITIAL_AD_FREQUENCY;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// This work only for android 4.4+
		if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

			getWindow().getDecorView().setSystemUiVisibility(flags);

			// Code below is to handle presses of Volume up or Volume down.
			// Without this, after pressing volume buttons, the navigation bar will
			// show up and won't hide
			final View decorView = getWindow().getDecorView();
			decorView.setOnSystemUiVisibilityChangeListener(visibility -> {
				if((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
					decorView.setSystemUiVisibility(flags);
				}
			});
		}

		setVolumeControlStream(AudioManager.STREAM_MUSIC);

		setContentView(R.layout.activity_main);

		mFrament = HomeFragment.newInstance();
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.gameContainer, mFrament).commitAllowingStateLoss();

		AdsHelper.init(this);
		AdsHelper.setActivity(this);
		AdsHelper.setLayout(R.id.adContainer);
		AdsHelper.setAdMobBannerUnitId(getResources().getString(R.string.admob_banner));
		AdsHelper.setAdMobInterstitialUnitId(getResources().getString(R.string.admob_int_ad));
		AdsHelper.setAppLovinSdkKey(getResources().getString(R.string.applovin_sdk_key));
		AdsHelper.prepareInterstitial();
		AdsHelper.setAdImpressionListener(amount -> this.runOnUiThread(() -> Toast.makeText(this, String.format("You were rewarded with %s %s", String.format("%.2f", amount / LUNAS_PER_NIM), EARN_UNITS), Toast.LENGTH_SHORT).show()));

		AdsHelper.checkPackageVersion(this);

		AdsHelper.showAd();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		//Log.i(TAG, "FrozenBubble.onSaveInstanceState()");
		/*
		 * Just have the View's thread save its state into our Bundle.
		 */
		super.onSaveInstanceState(outState);

		if (mFrament instanceof GameFragment) {
			((GameFragment)mFrament).saveState(outState);

			/*
			 * Set a flag indicating that the system passed us a bundle to save
			 * the game state in.
			 */
			SharedPreferences.Editor editor =
					PreferenceManager.getDefaultSharedPreferences(this).edit();
			editor.putBoolean("systemSave", true);
			editor.commit();
		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

	@Override
	public void onBackPressed() {
		if (mFrament instanceof GameFragment) {
			/*
			 * Only show the game exit dialog while a game is in progress,
			 * otherwise simply exit.
			 */
			if ((((GameFragment)mFrament).mGameThread != null) && ((GameFragment)mFrament).mGameThread.gameInProgress()) {
				((GameFragment)mFrament).pauseGameDialog();
			}
			else {
				((GameFragment)mFrament).exit();
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mFrament instanceof HomeFragment) {
			if (((HomeFragment)mFrament).myModPlayer != null) {
				((HomeFragment)mFrament).myModPlayer.pausePlay();
			}
		} else if (mFrament instanceof GameFragment) {
			((GameFragment)mFrament).pause();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mFrament instanceof HomeFragment) {
			if (((HomeFragment)mFrament).myModPlayer != null) {
				((HomeFragment)mFrament).restoreGamePrefs();
				if (((HomeFragment)mFrament).myPreferences.musicOn)
					((HomeFragment)mFrament).myModPlayer.unPausePlay();
			}
		}
	}

	@Override
	public void onWindowFocusChanged (boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (mFrament instanceof HomeFragment) {
			if (hasFocus) {
				((HomeFragment)mFrament).restoreGamePrefs();
			}
		} else if (mFrament instanceof GameFragment) {
			((GameFragment)mFrament).allowUnpause = hasFocus;
		}
		//if (hasFocus) {
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE
							| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
							| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
							| View.SYSTEM_UI_FLAG_FULLSCREEN
							| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		//}
	}

	@Override
	public void onGameEvent(GameScreen.eventEnum event) {
		if (!(mFrament instanceof GameFragment)) {
			return;
		}

		GameFragment gameFragment = (GameFragment)mFrament;

		switch (event) {
			case GAME_EXIT:
				/*
				 * Only show the game exit dialog while a game is in progress,
				 * otherwise simply exit.
				 */
				if ((gameFragment.mGameThread != null) && gameFragment.mGameThread.gameInProgress()) {
					gameFragment.pauseGameDialog();
				}
				else {
					gameFragment.exit();
				}
				break;

			case GAME_WON:
				showInterstitialAd();
				break;

			case GAME_LOST:
				showInterstitialAd();
				break;

			case GAME_PAUSED:
				gameFragment.saveGame(null);
				if (gameFragment.myModPlayer != null)
					gameFragment.myModPlayer.pausePlay();
				break;

			case GAME_RESUME:
				if (gameFragment.myModPlayer == null)
					gameFragment.playMusic(true);
				else if (gameFragment.allowUnpause)
					gameFragment.myModPlayer.unPausePlay();
				break;

			case GAME_START:
				if (!FrozenGame.arcadeGame && (gameFragment.mGameView != null) && (gameFragment.mGameThread != null)) {
					if (gameFragment.mGameThread.getCurrentLevelIndex() == 0) {
						/*
						 * Destroy the current music player, which will free audio
						 * stream resources and allow the system to use them.
						 */
						if (gameFragment.myModPlayer != null)
							gameFragment.myModPlayer.destroyMusicPlayer();
						gameFragment.myModPlayer = null;
						/*
						 * Clear the game screen and suspend input processing for
						 * three seconds.
						 *
						 * Afterwards,  the "About" screen will be displayed as a
						 * backup just in case anything goes awry with displaying
						 * the end-of-game credits.  It will be displayed after the
						 * user touches the screen when the credits are finished.
						 */
						gameFragment.mGameView.clearGameScreen(true, 3000);
						/*
						 * Create an intent to launch the activity to display the
						 * credits screen.
						 */
						Intent intent = new Intent(this, ScrollingCredits.class);
						startActivity(intent);
						break;
					}
				}
				gameFragment.playMusic(true);
				break;

			default:
				break;
		}
	}

	@Override
	public void onPlayerEvent(PlayerThread.eventEnum event) {
		if (!(mFrament instanceof GameFragment)) {
			return;
		}

		GameFragment gameFragment = (GameFragment)mFrament;

		switch (event) {
			case SONG_COMPLETED:
				if (gameFragment.myModPlayer != null) {
					if (!gameFragment.songsSequential()) {
						gameFragment.playMusic(true);
					}
				}
				break;
			case PATTERN_CHANGE:
			case PLAYER_STARTED:
			default:
				break;
		}
	}

	public static void showInterstitialAd() {
		if (--mInterstitialAdCounter <= 0) {
			mInterstitialAdCounter = INTERSTITIAL_AD_FREQUENCY;
			AdsHelper.showInterstitialAd();
		}
	}

	@Override
	public void onStartFrozenBubbleListener(boolean arcade) {
		mFrament = GameFragment.newInstance(arcade);
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.gameContainer, mFrament).commitAllowingStateLoss();
	}

	@Override
	public void onGoToHomeScreenListener() {
		mFrament = HomeFragment.newInstance();
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.gameContainer, mFrament).commitAllowingStateLoss();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		AdsHelper.finishActivityTracker();
	}
}
